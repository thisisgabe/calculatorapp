﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace CalculatorApp
{
    public partial class CalculatorAppPage : ContentPage
    {
        /*
         * Global Variables 
         */
        List<int> ListofNumbers = new List<int>();
        int CalcOp = 0;
        int Step = 0;
        /*
         * 0 = Add
         * 1 = Sub
         * 2 = Mult
         * 3 = Div
         */
        public CalculatorAppPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(object sender, System.EventArgs e)
        {
            Button myButton = (Button)sender;
            Debug.WriteLine($"The {myButton.Text} button was clicked");
            if (myButton.Text == "+")
            {
                Debug.WriteLine("Adding...");
                CalcOp = 0;
                string LabelText = labelDiplay.Text;
                int.TryParse(LabelText, out int LabelValue);
                ListofNumbers.Add(LabelValue);
                Step = 1;
                labelDiplay.Text = "0";

            }
            else if (myButton.Text == "-")
            {
                Debug.WriteLine("Subtracting...");
                CalcOp = 1;
                string LabelText = labelDiplay.Text;
                int.TryParse(LabelText, out int LabelValue);
                ListofNumbers.Add(LabelValue);
                Step = 1;
                labelDiplay.Text = "0";

            }
            else if (myButton.Text == "*")
            {
                Debug.WriteLine("Multiplying...");
                CalcOp = 2;
                string LabelText = labelDiplay.Text;
                int.TryParse(LabelText, out int LabelValue);
                ListofNumbers.Add(LabelValue);
                Step = 1;
                labelDiplay.Text = "0";

            }
            else if (myButton.Text == "/")
            {
                Debug.WriteLine("Dividing...");
                CalcOp = 3;
                string LabelText = labelDiplay.Text;
                int.TryParse(LabelText, out int LabelValue);
                ListofNumbers.Add(LabelValue);
                Step = 1;
                labelDiplay.Text = "0";

            }
            else if (myButton.Text == "C")
            {
                Debug.WriteLine("Clear all");
                CalcOp = 0;
                labelDiplay.Text = "0";
                ListofNumbers.Clear();

            }
            else if (myButton.Text == "=")
            {
                if(Step == 1)
                {
                    string LabelText = labelDiplay.Text;
                    int.TryParse(LabelText, out int LabelValue);
                    ListofNumbers.Add(LabelValue);
                    Step = 2;
                    Debug.WriteLine("Getting the result");
                    int finalsum = 0;
                    Debug.WriteLine($"First number = {ListofNumbers[0]}");
                    Debug.WriteLine($"Second number = {ListofNumbers[1]}");
                    Debug.WriteLine($"Operator = {CalcOp}");
                    switch(CalcOp)
                    {
                        case 0:
                            finalsum = ListofNumbers[0] + ListofNumbers[1];
                            Debug.WriteLine($"Final sum = {finalsum}");
                            labelDiplay.Text = finalsum.ToString();
                            break;
                        case 1:
                            finalsum = ListofNumbers[0] - ListofNumbers[1];
                            Debug.WriteLine($"Final diff = {finalsum}");
                            labelDiplay.Text = finalsum.ToString();
                            break;
                        case 2:
                            finalsum = ListofNumbers[0] * ListofNumbers[1];
                            Debug.WriteLine($"Final product = {finalsum}");
                            labelDiplay.Text = finalsum.ToString();
                            break;
                        case 3:
                            finalsum = ListofNumbers[0] / ListofNumbers[1];
                            Debug.WriteLine($"Final quotient = {finalsum}");
                            labelDiplay.Text = finalsum.ToString();
                            break;
                            
                    }
                    ListofNumbers.Clear();
                }


            }
            //all the other buttons (we are just entering data)
            else
            {
                //are we starting new?
                //if so then clear everything
                if(Step==2)
                {
                    labelDiplay.Text = myButton.Text;
                    Step = 0;
                }
                else
                {
                    if (labelDiplay.Text == "0")
                    {
                        labelDiplay.Text = myButton.Text;
                    }
                    else
                    {
                        labelDiplay.Text = labelDiplay.Text + myButton.Text;
                    } 
                }

            }
        }
    }
}
